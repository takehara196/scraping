# !/usr/bin/env python
# -*- coding:utf-8 -*-

"""
    リクナビスクレイピング
    リクナビから会社名, 従業員数, 所在地, 設立, 資本金, 売上高, 企業IDを取得する
    python --version: Python 3.7.2
    created: 2019-07-11
    Auther: S.TAKEHARA
"""

import math
import pandas as pd
import urllib.request
from bs4 import BeautifulSoup
from bs4.element import Tag

# User-Agent
headers = {"User-Agent": "Example Company, example@example.com"}


# 全掲載社数取得, 掲載ページ数計算
def _company_counter_result():
    # 全掲載社数取得ページURL
    url = "https://job.rikunabi.com/2020/s/?isc=r20rcnc01601"
    # 対象のコンテキストマネージャを実際にリクエスト
    re = urllib.request.Request(url, None, headers)
    html = urllib.request.urlopen(re)
    # 受け取ったサイトのコンテキストマネージャをhtmlにパース
    soup = BeautifulSoup(html, "html.parser")
    # 全掲載社数取得, intに変換
    company_counter_result = int(soup.find(class_="ts-h-search-companyCounterResult").text)
    # ループするページ数
    pages = math.floor((company_counter_result / 100) + 1)
    # pages = 5
    return pages


# 企業ID取得
def _get_value(pages):
    value_list = []

    # 1ページ目からpagesページ目まで
    for n in range(1, pages + 1):
        print(n)
        # スクレイピング対象のURL
        url = "https://job.rikunabi.com/2020/s/?isc=ps054&pn={}".format(n)
        try:
            # 対象のコンテキストマネージャを実際にリクエスト
            re = urllib.request.Request(url, None, headers)
            html = urllib.request.urlopen(re)
            # 受け取ったサイトのコンテキストマネージャをhtmlにパース
            soup = BeautifulSoup(html, "html.parser")
            # ts-h-search-cassetteクラス直下のinput要素を取得
            company_value = soup.select(".ts-h-search-cassette > input")
            for vc in company_value:
                # rから始まる企業ID
                if vc.get("value").startswith("r"):
                    c_value = vc.get("value")
                    value_list.append(c_value)
                # mから始まる企業ID
                elif vc.get("value").startswith("m"):
                    c_value = vc.get("value")
                    value_list.append(c_value)
                else:
                    pass
        except:
            pass
    return value_list


# 企業詳細ページから会社名と従業員数取得
def _get_jobinfo(value_list):
    company_names = []
    company_infos = []
    address_infos = []
    establishment_infos = []
    capitalstock_infos = []
    amountsales_infos = []


    count = 0
    for i in value_list:
        count += 1
        print(count)
        # strに変換
        i = str(i)
        # スクレイピング対象のURL
        url = "https://job.rikunabi.com/2020/company/{}/".format(i)
        try:
            # 対象のコンテキストマネージャを実際にリクエスト
            re = urllib.request.Request(url, None, headers)
            html = urllib.request.urlopen(re)
            # 受け取ったサイトのコンテキストマネージャをhtmlにパース
            soup = BeautifulSoup(html, "html.parser")
            # 会社名取得
            company_name = soup.find("h1").text
            # 会社名リストに追加
            company_names.append(company_name)
            # データフレームに格納
            df_company_names = pd.DataFrame(company_names, columns=['会社名'])
            elems = soup.find_all(class_="ts-h-mod-dataTable02")

            td_list = []
            address_list = []
            td_establishment_list = []
            td_capitalstock_list = []
            td_amountsales_list = []

            import re
            for elem in elems:

                # 項目をリストに格納
                employee_list = []

                elem_th = elem.find_all("th")
                for th in elem_th:
                    employee_list.append(th.text)

                # 重複する要素を抽出, リストに格納
                employee_list_duplicate = [x for x in set(employee_list) if employee_list.count(x) > 1]

                for th in elem_th:
                    # 重複要素リストに"従業員数"が含まれる場合
                    if "従業員数" in employee_list_duplicate:
                        pass
                    # 重複要素リストに"従業員数"が含まれない場合
                    else:
                        if th.text == "従業員数":
                            for td in th.next_siblings:
                                if type(td) == Tag:
                                    td_list.append(td.text + "\n")
                                else:
                                    # 従業員数が含まれない場合
                                    pass
                        else:
                            pass

                    # # 重複要素リストに"社員数"が含まれる場合
                    # if "社員数" in employee_list_duplicate:
                    #     pass
                    # # 重複要素リストに"社員数"が含まれない場合
                    # else:
                    #     if th.text == "社員数":
                    #         for td in th.next_siblings:
                    #             if type(td) == Tag:
                    #                 td_list.append(td.text + "\n")
                    #     else:
                    #         pass
                    #
                    # # 重複要素リストに"スタッフ数"が含まれる場合
                    # if "スタッフ数" in employee_list_duplicate:
                    #     pass
                    # # 重複要素リストに"スタッフ数"が含まれない場合
                    # else:
                    #     if th.text == "スタッフ数":
                    #         for td in th.next_siblings:
                    #             if type(td) == Tag:
                    #                 td_list.append(td.text + "\n")
                    #     else:
                    #         pass
                    #
                    # # 重複要素リストに"スタッフ数"が含まれる場合
                    # if "各社データ" in employee_list_duplicate:
                    #     pass
                    # # 重複要素リストに"各社データ"が含まれない場合
                    # else:
                    #     if th.text == "各社データ":
                    #         for td in th.next_siblings:
                    #             if type(td) == Tag:
                    #                 td_list.append(td.text + "\n")
                    #     else:
                    #         pass

                company_infos.append(td_list)

            for elem in elems:
                # 項目をリストに格納
                section_list = []

                elem_th = elem.find_all("th")
                for th in elem_th:
                    section_list.append(th.text)

                # 重複要素を抽出
                section_list_duplicate = [x for x in set(section_list) if section_list.count(x) > 1]

                for th in elem_th:
                    # 重複要素リストに"事業所"が含まれる場合
                    if "事業所" in section_list_duplicate:
                        pass
                    # 重複要素リストに"事業所"が含まれない場合
                    else:
                        if th.text == "事業所":
                            for td in th.next_siblings:
                                if type(td) == Tag:
                                    address_list.append(td.text + "\n")
                                else:
                                    pass
                        else:
                            pass

                    # # 重複要素リストに"事業所"が含まれる場合
                    # if "拠点" in section_list_duplicate:
                    #     pass
                    # # 重複要素リストに"事業所"が含まれない場合
                    # else:
                    #     if th.text == "拠点":
                    #         for td in th.next_siblings:
                    #             if type(td) == Tag:
                    #                 address_list.append(td.text + "\n")
                    #     else:
                    #         pass

                address_infos.append(address_list)

            # 設立
            for elem in elems:

                # 項目をリストに格納
                establishment_list = []

                elem_th = elem.find_all("th")
                for th in elem_th:
                    establishment_list.append(th.text)

                # 重複する要素を抽出, リストに格納
                establishment_list_duplicate = [x for x in set(establishment_list) if establishment_list.count(x) > 1]

                for th in elem_th:
                    # 重複要素リストに"設立"が含まれる場合
                    if "設立" in establishment_list_duplicate:
                        pass
                    # 重複要素リストに"設立"が含まれない場合
                    else:
                        if th.text == "設立":
                            for td in th.next_siblings:
                                if type(td) == Tag:
                                    td_establishment_list.append(td.text + "\n")
                                else:
                                    pass
                        else:
                            pass

                establishment_infos.append(td_establishment_list)

            # 資本金
            for elem in elems:

                # 項目をリストに格納
                capitalstock_list = []

                elem_th = elem.find_all("th")
                for th in elem_th:
                    capitalstock_list.append(th.text)

                # 重複する要素を抽出, リストに格納
                capitalstock_list_duplicate = [x for x in set(capitalstock_list) if capitalstock_list.count(x) > 1]

                for th in elem_th:
                    # 重複要素リストに"資本金"が含まれる場合
                    if "資本金" in capitalstock_list_duplicate:
                        pass
                    # 重複要素リストに"資本金"が含まれない場合
                    else:
                        if th.text == "資本金":
                            for td in th.next_siblings:
                                if type(td) == Tag:
                                    td_capitalstock_list.append(td.text + "\n")
                                else:
                                    pass
                        else:
                            pass

                capitalstock_infos.append(td_capitalstock_list)

            # 売上高
            for elem in elems:

                # 項目をリストに格納
                amountsales_list = []

                elem_th = elem.find_all("th")
                for th in elem_th:
                    amountsales_list.append(th.text)

                # 重複する要素を抽出, リストに格納
                amountsales_list_duplicate = [x for x in set(amountsales_list) if amountsales_list.count(x) > 1]

                for th in elem_th:
                    # 重複要素リストに"売上高"が含まれる場合
                    if "売上高" in amountsales_list_duplicate:
                        pass
                    # 重複要素リストに"売上高"が含まれない場合
                    else:
                        if th.text == "売上高":
                            for td in th.next_siblings:
                                if type(td) == Tag:
                                    td_amountsales_list.append(td.text + "\n")
                                else:
                                    pass
                        else:
                            pass

                amountsales_infos.append(td_amountsales_list)

            df_company_infos = pd.DataFrame(company_infos, columns=['従業員数'])
            df_address_infos = pd.DataFrame(address_infos, columns=['事業所'])
            df_establishment_infos = pd.DataFrame(establishment_infos, columns=['設立'])
            df_capitalstock_infos = pd.DataFrame(capitalstock_infos, columns=['資本金'])
            df_amountsales_infos = pd.DataFrame(amountsales_infos, columns=['売上高'])

        except:
            pass

        df_value_list = pd.DataFrame(value_list, columns=['企業ID'])

    return df_company_names, df_company_infos, df_address_infos, df_establishment_infos, df_capitalstock_infos, df_amountsales_infos, df_value_list


# 会社リストと従業員数リストを結合
def _merge(df_company_names, df_company_infos, df_address_infos, df_establishment_infos, df_capitalstock_infos,
           df_amountsales_infos, df_value_list):
    df = pd.concat([df_company_names, df_company_infos], axis=1)
    df2 = pd.concat([df, df_address_infos], axis=1)  # 事業所
    df3 = pd.concat([df2, df_establishment_infos], axis=1)  # 設立
    df4 = pd.concat([df3, df_capitalstock_infos], axis=1)  # 資本金
    df5 = pd.concat([df4, df_amountsales_infos], axis=1)  # 売上高
    df6 = pd.concat([df5, df_value_list], axis=1)  # 企業ID
    # CSV出力
    df6.to_csv("./rikunabi_scraping.csv", index=None)


# main関数
def main():
    pages = _company_counter_result()
    value_list = _get_value(pages)
    df_company_names, df_company_infos, df_address_infos, df_establishment_infos, df_capitalstock_infos, df_amountsales_infos, df_value_list = _get_jobinfo(
        value_list)
    _merge(df_company_names, df_company_infos, df_address_infos, df_establishment_infos, df_capitalstock_infos,
           df_amountsales_infos, df_value_list)


if __name__ == '__main__':
    main()
