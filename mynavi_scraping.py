# !/usr/bin/env python
# -*- coding:utf-8 -*-

"""
    マイナビスクレイピング
    マイナビから会社名, 従業員数, 所在地, 設立, 資本金, 売上高, 会社詳細ページURLを取得する
    python --version: Python 3.7.2
    created: 2019-07-15
    Auther: S.TAKEHARA
"""

import math
import pandas as pd
import urllib.request
from bs4 import BeautifulSoup
from bs4.element import Tag

# User-Agent
headers = {"User-Agent": "Example Company, example@example.com"}


# 全掲載社数取得
def _company_counter_result():
    # 全掲載社数取得ページURL
    url = "https://tenshoku.mynavi.jp/search/list"

    # 対象のコンテキストマネージャを実際にリクエスト
    re = urllib.request.Request(url, None, headers)
    html = urllib.request.urlopen(re)
    # 受け取ったサイトのコンテキストマネージャをhtmlにパース
    soup = BeautifulSoup(html, "html.parser")
    # 全掲載社数取得, intに変換
    company_counter_result = int(soup.find(class_="js__searchRecruit--count").text)
    print(company_counter_result)
    # ループするページ数
    pages = math.floor((company_counter_result / 50) + 1)
    # テスト実行
    # pages = 10
    return pages


global_url = "https://tenshoku.mynavi.jp/"


# 会社紹介一覧ページから各会社詳細ページURL取得
def _get_value(pages):
    url_list = []
    # 1ページ目からpagesページ目まで
    for n in range(1, pages + 1):
        print(n)
        # スクレイピング対象のURL
        url = "https://tenshoku.mynavi.jp/search/list/?pageNum={}".format(n)

        try:
            # 対象のコンテキストマネージャを実際にリクエスト
            re = urllib.request.Request(url, None, headers)
            html = urllib.request.urlopen(re)
            # 受け取ったサイトのコンテキストマネージャをhtmlにパース
            soup = BeautifulSoup(html, "html.parser")
            # ts-h-search-cassetteクラス直下のinput要素を取得
            company_value = soup.select(".cassetteRecruit__bottom > a")

            for vc in company_value:
                # URL取得
                c_value = vc.get("href")
                # /で分割
                split_url = c_value.split("/")
                url_list.append(split_url)
        except:
            pass

    return url_list


# 項目取得
def _get_jobinfo(url_list):
    count = 0

    url_page_list = []
    company_name_list = []
    employee_count_list = []
    capital_stock_list = []
    amount_sales_list = []
    establishment_list = []
    address_list = []

    for i in url_list:
        count += 1
        print(count)

        url = global_url + i[1]

        # 会社詳細ページのURL一覧リスト
        url_page_list.append(url)

        # 対象のコンテキストマネージャを実際にリクエスト
        re = urllib.request.Request(url, None, headers)
        html = urllib.request.urlopen(re)
        # 受け取ったサイトのコンテキストマネージャをhtmlにパース
        soup = BeautifulSoup(html, "html.parser")

        import re
        try:
            """会社名取得"""
            elems = soup.find_all(class_="cassetteOfferRecapitulate__content")
            for elem in elems:
                company_name = elem.find('p', class_="text").text

                if "|" in company_name:
                    # "|"以前の文字列を抽出
                    company_name = re.search(r'.*\|', company_name, flags=re.DOTALL)
                    company_name = company_name.group(0)

                    # " |"を削除
                    company_name = company_name.split("|")[0]

                    # 前後のスペースを削除
                    company_name = company_name.strip()

                    company_name_list.append(company_name)

                elif "｜" in company_name:
                    # "|"以前の文字列を抽出
                    company_name = re.search(r'.*｜', company_name, flags=re.DOTALL)
                    company_name = company_name.group(0)

                    # " |"を削除
                    company_name = company_name.split("｜")[0]

                    # 前後のスペースを削除
                    company_name = company_name.strip()

                    company_name_list.append(company_name)

                else:
                    company_name_list.append(company_name)

        except:
            pass

        emp_elem_list = []
        cap_stck_elem_list = []
        amunt_sls_elems_list = []
        estblshmnt_elems_list = []
        addr_elems_list = []

        try:
            """従業員数取得"""
            employee_elems = soup.find_all(class_="jobOfferTable__head")
            for emp_elem in employee_elems:
                emp_elem_list.append(emp_elem.text)

            if "従業員数" in emp_elem_list:
                for emp_elem in employee_elems:
                    if emp_elem.text == "従業員数":
                        for employee_count in emp_elem.next_siblings:
                            if type(employee_count) == Tag:
                                # 改行コード削除
                                emp_count = employee_count.text.strip()
                                # リストに追加
                                employee_count_list.append(emp_count)
            else:
                employee_count_list.append("")
        except:
            pass

        try:
            """資本金取得"""
            capital_stock_elems = soup.find_all(class_="jobOfferTable__head")
            for cap_stck_elems in capital_stock_elems:
                cap_stck_elem_list.append(cap_stck_elems.text)

            if "資本金" in cap_stck_elem_list:
                for cap_stck_elems in capital_stock_elems:
                    if cap_stck_elems.text == "資本金":
                        for capital_stock in cap_stck_elems.next_siblings:
                            if type(capital_stock) == Tag:
                                # 改行コード削除
                                emp_count = capital_stock.text.strip()
                                # リストに追加
                                capital_stock_list.append(emp_count)
            else:
                capital_stock_list.append("")
        except:
            pass

        try:
            """売上高取得"""
            amount_of_sales_elems = soup.find_all(class_="jobOfferTable__head")
            for amunt_sls_elems in amount_of_sales_elems:
                amunt_sls_elems_list.append(amunt_sls_elems.text)

            if "売上高" in amunt_sls_elems_list:
                for amunt_sls_elems in amount_of_sales_elems:
                    if amunt_sls_elems.text == "売上高":
                        for amount_of_sales in amunt_sls_elems.next_siblings:
                            if type(amount_of_sales) == Tag:
                                # 改行コード削除
                                emp_count = amount_of_sales.text.strip()
                                # リストに追加
                                amount_sales_list.append(emp_count)
            else:
                amount_sales_list.append("")
        except:
            pass

        try:
            """設立"""
            establishment_elems = soup.find_all(class_="jobOfferTable__head")
            for estblshmnt_elems in establishment_elems:
                estblshmnt_elems_list.append(estblshmnt_elems.text)

            if "設立" in estblshmnt_elems_list:
                for estblshmnt_elems in establishment_elems:
                    if estblshmnt_elems.text == "設立":
                        for establishment in estblshmnt_elems.next_siblings:
                            if type(establishment) == Tag:
                                # 改行コード削除
                                emp_count = establishment.text.strip()
                                # リストに追加
                                establishment_list.append(emp_count)
            else:
                establishment_list.append("")
        except:
            pass

        try:
            """本社所在地"""
            address_elems = soup.find_all(class_="jobOfferTable__head")
            for addr_elems in address_elems:
                addr_elems_list.append(addr_elems.text)

            if "本社所在地" in addr_elems_list:
                for addr_elems in address_elems:
                    if addr_elems.text == "本社所在地":
                        for address in addr_elems.next_siblings:
                            if type(address) == Tag:
                                # 改行コード削除
                                emp_count = address.text.strip()
                                # リストに追加
                                address_list.append(emp_count)
            else:
                address_list.append("")
        except:
            pass

    df_url_page_list = pd.DataFrame(url_page_list, columns=['詳細ページURL'])
    df_company_name_list = pd.DataFrame(company_name_list, columns=["会社名"])
    df_employee_count_list = pd.DataFrame(employee_count_list, columns=["従業員数"])
    df_capital_stock_list = pd.DataFrame(capital_stock_list, columns=["資本金"])
    df_amount_sales_list = pd.DataFrame(amount_sales_list, columns=["売上高"])
    df_establishment_list = pd.DataFrame(establishment_list, columns=["設立"])
    df_address_list = pd.DataFrame(address_list, columns=["本社所在地"])

    return df_company_name_list, df_employee_count_list, df_capital_stock_list, df_amount_sales_list, df_establishment_list, df_address_list, df_url_page_list


# 結合
def _merge(df_company_name_list, df_employee_count_list, df_capital_stock_list, df_amount_sales_list,
           df_establishment_list, df_address_list, df_url_page_list):
    df = pd.concat([df_company_name_list, df_employee_count_list], axis=1)
    df2 = pd.concat([df, df_capital_stock_list], axis=1)  # 資本金リスト結合
    df3 = pd.concat([df2, df_amount_sales_list], axis=1)  # 売上高リスト結合
    df4 = pd.concat([df3, df_establishment_list], axis=1)  # 設立リスト結合
    df5 = pd.concat([df4, df_address_list], axis=1)  # 所在地リスト結合
    df6 = pd.concat([df5, df_url_page_list], axis=1)  # 会社詳細URLリスト結合
    # CSV出力
    df6.to_csv("./mynavi_scraping.csv", index=None)


# main関数
def main():
    pages = _company_counter_result()
    url_list = _get_value(pages)
    df_company_name_list, df_employee_count_list, df_capital_stock_list, df_amount_sales_list, df_establishment_list, df_address_list, df_url_page_list = _get_jobinfo(
        url_list)
    _merge(df_company_name_list, df_employee_count_list, df_capital_stock_list, df_amount_sales_list,
           df_establishment_list, df_address_list, df_url_page_list)


if __name__ == '__main__':
    main()
